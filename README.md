# Sorted Linked List
This repository demonstrates how to use SortedLinkedList.

## What is a sorted linked list?
A linked list is a sequential collection of data elements connected via links. 
The data element of a linked list is known as a node which contains two parts namely- the data part and the pointer.

## Requiremets
Minimum required PHP version is **8.1**

## Usage
SortedLinkedList provides two types of list:
- IntegerSortedLinkedList for integer list values
- StringSortedLinkedList for string list values

Basic initialization of list:
```php
use SortedLinkedList\StringSortedLinkedList;

$list = new StringSortedLinkedList();

$list->add('xavier');
```
or add values in bulk
```php
$list->addBulk(['adam', 'text']);
```
similarly, removing elements from the list
```php
$list->remove('xavier');
$list->removeBulk(['adam', 'text']);
```
Wrong initialization of list:
```php
$list = new StringSortedLinkedList();
$list->add(10); //throw Exception, expected value is string
```

Use class Setting for changing order or other settings of the list
```php
use SortedLinkedList\Setting\Order;
use SortedLinkedList\Setting\Setting;

$settings = new Setting(Order::descending);
$list = new StringSortedLinkedList($settings);
```

SortedLinkedList implements \Iterator, \Countable interface so you can use:

```php
echo "count {$list->count()} \n";

foreach ($list as $key => $node) {
    echo "$key: {$node->getValue()} \n";
}
```
Every single item of the list is simple class Node, which extends AbstractNode and implements NodeInterface. 
Node contains value and reference to next Node.
```php
foreach ($list as $node) {
    $value = $node->getValue();
    $nextNode = $node->getNextNode();
}
```
For obtain Node from list by value or checking if list contain value:
```php
$xavier = $list->getNodeByValue('xavier'); //return SortedLinkedList\String\Node
$nonExistNode = $list->getNodeByValue('sara'); //return null
```
## More examples
For more examples see [example dir](example)

## Advanced settings
Class Setting is used for settings of list, available parameters:
- order, order/sort item of list, default value ascending (Order::ascending)
- uniqueValues, list contains Nodes with unique values, default value false
- removeAllNodesWithValue, remove all Nodes contains value, default value false

```php
$setting = new Setting(
    order: Order::descending,
    uniqueValues: true
);
$list = new StringSortedLinkedList($setting);

$list->add('ahoj');
$list->add('xavier');
$list->add('ahoj'); //throw Exception

```
```php
$setting = new Setting(
    order: Order::descending,
    uniqueValues: false
);
$list = new StringSortedLinkedList($setting);

$list->add('ahoj');
$list->add('xavier');
$list->add('ahoj'); //ok, add Node into list

```
