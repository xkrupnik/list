<?php

require_once __DIR__ . '/../vendor/autoload.php';

use SortedLinkedList\DateTimeSortedLinkedList;


$list = new DateTimeSortedLinkedList();
$list->add(DateTime::createFromFormat('Y-m-d', '2000-11-08'));
$list->addBulk([
    new DateTime('2023-03-04'),
    new DateTime('2022-05-20 22:47')
]);
echo "count {$list->count()} \n";

foreach ($list as $key => $node) {
    echo "$key: {$node->getValue()->format('d.m.Y H:i')} \n";
}
