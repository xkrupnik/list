<?php

require_once __DIR__ . '/../vendor/autoload.php';

use SortedLinkedList\IntegerSortedLinkedList;


$list = new IntegerSortedLinkedList();
$list->add(8);
$list->add(9);
$list->add(4);
$list->addBulk([-5, 333, 0, 66]);
$list->remove(4);

echo "count {$list->count()} \n";

foreach ($list as $key => $node) {
    echo "$key: {$node->getValue()} \n";
}

