<?php

require_once __DIR__ . '/../vendor/autoload.php';

use SortedLinkedList\StringSortedLinkedList;
use SortedLinkedList\Setting\Setting;
use SortedLinkedList\Setting\Order;

$setting = new Setting(
    order: Order::descending,
    removeAllNodesWithValue: true
);
$list = new StringSortedLinkedList($setting);

$list->add('ahoj');
$list->add('text');
$list->add('xavier');
$list->add('ahoj');
$list->addBulk(['žofie', 'znojmo', 'adam']);
$list->remove('ahoj');

echo "count {$list->count()} \n";

foreach ($list as $key => $node) {
    echo "$key: {$node->getValue()} \n";
}

echo "searched node ";
$node = $list->getNodeByValue('ahoj');
//print_r($node);

