<?php
declare(strict_types=1);

namespace SortedLinkedList;

use SortedLinkedList\Node\AbstractNode;
use SortedLinkedList\Node\NodeFactoryInterface;
use SortedLinkedList\Setting\Order;
use SortedLinkedList\Setting\Setting;


abstract class AbstractSortedLinkedList implements \Iterator, \Countable
{
    protected ?AbstractNode $node = null;
    protected ?AbstractNode $currentNode = null;

    protected int $position = 0;
    protected int $count = 0;

    public function __construct(
        protected Setting $setting,
        protected NodeFactoryInterface $nodeFactory
    ) {}

    public function add(mixed $value): void
    {
        if ($this->setting->uniqueValues) {
            $existsNode = $this->getNodeByValue($value);
            if ($existsNode) {
                throw new \LogicException("List node with value already exists, value: $value");
            }
        }
        $node = $this->nodeFactory->createNode($value);
        $this->addNode($node);
    }

    public function addBulk(array $values): void
    {
        foreach ($values as $value) {
            $this->add($value);
        }
    }

    public function remove(mixed $value): void
    {
        if ($this->setting->removeAllNodesWithValue) {
            $node = $this->getNodeByValue($value);
            while ($node) {
                $this->removeNodeByValue($value);
                $node = $this->getNodeByValue($value);
            }
        } else {
            $this->removeNodeByValue($value);
        }
    }

    public function removeBulk(array $values): void
    {
        foreach ($values as $value) {
            $this->remove($value);
        }
    }

    public function getNodeByValue(mixed $value): ?AbstractNode
    {
        foreach ($this as $node) {
            if ($node->getValue() === $value) {
                return $node;
            }
        }
        return null;
    }

    protected function addNode(AbstractNode $newNode): void
    {
        ++$this->count;

        if (!$this->node) {
            $this->node = $newNode;
            return;
        }

        $currentNode = $this->node;
        $prevNode = null;

        while ($currentNode) {

            $cmp = $this->cmpResolve($newNode, $currentNode);
            if ($cmp) {
                $newNode->setNextNode($currentNode);
                if ($prevNode === null) {
                    $this->node = $newNode;
                } else {
                    $prevNode->setNextNode($newNode);
                }
                return;
            }

            if (!$currentNode->getNextNode()) {
                $currentNode->setNextNode($newNode);
                return;
            }

            $prevNode = $currentNode;
            $currentNode = $currentNode->getNextNode();

        }
    }

    protected function removeNodeByValue($value): void
    {
        if (!$this->node) {
            return;
        }
        --$this->count;

        $currentNode = $this->node;
        $prevNode = null;

        while ($currentNode) {

            if ($currentNode->getValue() === $value) {
                if ($prevNode) {
                    $prevNode->setNextNode($currentNode->getNextNode());
                    return;
                }
                $this->node = $currentNode->getNextNode();
                return;
            }

            $prevNode = $currentNode;
            $currentNode = $currentNode->getNextNode();

        }
    }

    public function cmp(
        AbstractNode $aNode,
        AbstractNode $bNode
    ): int
    {
        return $aNode->getValue() <=> $bNode->getValue();
    }

    private function cmpResolve(
        AbstractNode $aNode,
        AbstractNode $bNode
    ): bool
    {
        $cmpResult = $this->cmp($aNode, $bNode);
        if ($this->setting->order === Order::descending) {
            return $cmpResult > 0;
        }
        return $cmpResult < 0;
    }

    /**** Iterator implements *****/

    public function current(): AbstractNode
    {
        return $this->currentNode;
    }

    public function next(): void
    {
        ++$this->position;
        if ($this->currentNode) {
            $this->currentNode = $this->currentNode->getNextNode();
        }
    }

    public function key(): int
    {
        return $this->position;
    }

    public function valid(): bool
    {
        return $this->currentNode !== null;
    }

    public function rewind(): void
    {
        $this->position = 0;
        $this->currentNode = $this->node;
    }

    public function count(): int
    {
        return $this->count;
    }
}