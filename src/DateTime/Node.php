<?php
declare(strict_types=1);

namespace SortedLinkedList\DateTime;

use SortedLinkedList\Node\AbstractNode;
use DateTime;

class Node extends AbstractNode
{

    public function __construct(
        private DateTime $value,
        private ?Node $nextNode = null
    ) {}

    public function getValue(): DateTime
    {
        return $this->value;
    }

    public function getNextNode(): ?Node
    {
        return $this->nextNode;
    }

    public function setNextNode(?AbstractNode $nextNode)
    {
        $this->nextNode = $nextNode;
    }

}