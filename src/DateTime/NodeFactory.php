<?php
declare(strict_types=1);

namespace SortedLinkedList\DateTime;

use SortedLinkedList\Node\AbstractNode;
use SortedLinkedList\Node\NodeFactoryInterface;


class NodeFactory implements NodeFactoryInterface
{

    /**
     * @return Node
     */
   public function createNode(mixed $value): AbstractNode
   {
       return new Node($value);
   }
}