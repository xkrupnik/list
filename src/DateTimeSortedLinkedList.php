<?php
declare(strict_types=1);

namespace SortedLinkedList;

use SortedLinkedList\DateTime\NodeFactory;
use SortedLinkedList\Setting\Setting;

class DateTimeSortedLinkedList extends AbstractSortedLinkedList
{

    public function __construct(?Setting $setting = null)
    {
        $nodeFactory = new NodeFactory();

        parent::__construct($setting ?? new Setting(), $nodeFactory);
    }

}