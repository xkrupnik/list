<?php
declare(strict_types=1);

namespace SortedLinkedList\Integer;

use SortedLinkedList\Node\AbstractNode;

class Node extends AbstractNode
{

    public function __construct(
        private int $value,
        private ?Node $nextNode = null
    ) {}

    public function getValue(): int
    {
        return $this->value;
    }

    public function getNextNode(): ?Node
    {
        return $this->nextNode;
    }

    public function setNextNode(?AbstractNode $nextNode)
    {
        $this->nextNode = $nextNode;
    }

}