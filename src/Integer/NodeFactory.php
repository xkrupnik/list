<?php
declare(strict_types=1);

namespace SortedLinkedList\Integer;

use SortedLinkedList\Node\AbstractNode;
use SortedLinkedList\Node\NodeFactoryInterface;


class NodeFactory implements NodeFactoryInterface
{

    /**
     * @param int $value
     * @return Node
     */
   public function createNode(mixed $value): AbstractNode
   {
       if (!is_int($value)) {
           throw new \InvalidArgumentException("IntegerSortedLinkedList only accepts integers. Input was: $value - " . gettype($value));
       }
       return new Node($value);
   }
}