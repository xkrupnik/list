<?php
declare(strict_types=1);

namespace SortedLinkedList;

use SortedLinkedList\Integer\NodeFactory;
use SortedLinkedList\Setting\Setting;

class IntegerSortedLinkedList extends AbstractSortedLinkedList
{

    public function __construct(
        ?Setting $setting = null
    ) {
        $nodeFactory = new NodeFactory();
        $setting = $setting ?? new Setting();

        parent::__construct($setting, $nodeFactory);
    }

}