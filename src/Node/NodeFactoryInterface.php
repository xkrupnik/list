<?php
declare(strict_types=1);

namespace SortedLinkedList\Node;

interface NodeFactoryInterface
{

    public function createNode(mixed $value): AbstractNode;

}