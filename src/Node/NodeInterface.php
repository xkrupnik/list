<?php
declare(strict_types=1);

namespace SortedLinkedList\Node;

interface NodeInterface
{

    public function getValue();

    public function getNextNode();

    public function setNextNode(?AbstractNode $nextNode);

}