<?php

namespace SortedLinkedList\Setting;

enum Order: string
{
    case ascending = 'asc';
    case descending = 'desc';
}