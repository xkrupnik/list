<?php
declare(strict_types=1);

namespace SortedLinkedList\Setting;

class Setting
{

    public function __construct(
        public readonly Order $order = Order::ascending,
        public readonly bool $uniqueValues = false,
        public readonly bool $removeAllNodesWithValue = false
    ) {}

}