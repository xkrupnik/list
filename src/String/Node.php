<?php
declare(strict_types=1);

namespace SortedLinkedList\String;

use SortedLinkedList\Node\AbstractNode;


class Node extends AbstractNode
{

    public function __construct(
        private string $value,
        private ?Node  $nextNode = null
    ) {}

    public function getValue(): string
    {
        return $this->value;
    }

    public function getNextNode(): ?Node
    {
        return $this->nextNode;
    }

    public function setNextNode(?AbstractNode $nextNode)
    {
        $this->nextNode = $nextNode;
    }

}