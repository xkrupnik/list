<?php
declare(strict_types=1);

namespace SortedLinkedList\String;

use SortedLinkedList\Node\AbstractNode;
use SortedLinkedList\Node\NodeFactoryInterface;


class NodeFactory implements NodeFactoryInterface
{

    /**
     * @param string $value
     * @return Node
     */
   public function createNode(mixed $value): AbstractNode
   {
       if (!is_string($value)) {
           throw new \InvalidArgumentException("StringSortedLinkedList only accepts strings. Input was: $value - " . gettype($value));
       }
       return new Node($value);
   }
}