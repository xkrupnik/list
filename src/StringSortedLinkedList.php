<?php
declare(strict_types=1);

namespace SortedLinkedList;

use SortedLinkedList\Node\AbstractNode;
use SortedLinkedList\Setting\Setting;
use SortedLinkedList\String\NodeFactory;


class StringSortedLinkedList extends AbstractSortedLinkedList
{

    public function __construct(
       ?Setting $setting = null
    ) {
        $nodeFactory = new NodeFactory();
        $setting = $setting ?? new Setting();

        parent::__construct($setting, $nodeFactory);
    }

    public function cmp(AbstractNode $aNode, AbstractNode $bNode): int
    {
        return strcmp($aNode->getValue(), $bNode->getValue());
    }

}