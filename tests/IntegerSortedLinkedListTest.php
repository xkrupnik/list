<?php

namespace SortedLinkedList;

use PHPUnit\Framework\TestCase;
use SortedLinkedList\Setting\Order;
use SortedLinkedList\Setting\Setting;
use SortedLinkedList\Integer\Node;

final class IntegerSortedLinkedListTest extends TestCase
{

    public function testCreatedWithValidValues(): void
    {
        $list = new IntegerSortedLinkedList();
        $list->add(11);
        $list->add(666);
        $list->add(-587);
        $list->add(0);

        $this->assertSame(4, $list->count());

        foreach ($list as $node) {
            $this->assertInstanceOf(Node::class, $node);
            $this->assertIsInt($node->getValue());
        }
    }

    public function testCreatedWithInvalidValues(): void
    {
        $this->expectException(\InvalidArgumentException::class);

        $list = new IntegerSortedLinkedList();
        $list->add(11);
        $list->add(666);
        $list->add('adam');
        $list->add(0);
    }

    public function testOrderAsc(): void
    {
        $list = new IntegerSortedLinkedList();
        $list->add(11);
        $list->add(666);
        $list->add(-587);
        $list->add(0);

        foreach ($list as $key => $node) {
            $value = $node->getValue();
            switch ($key) {
                case 0:
                    $this->assertSame(-587, $value);
                    break;
                case 1:
                    $this->assertSame(0, $value);
                    break;
                case 2:
                    $this->assertSame(11, $value);
                    break;
                case 3:
                    $this->assertSame(666, $value);
                    break;
            }
        }
    }

    public function testOrderDesc(): void
    {
        $settings = new Setting(Order::descending);
        $list = new IntegerSortedLinkedList($settings);
        $list->add(11);
        $list->add(666);
        $list->add(-587);
        $list->add(0);

        foreach ($list as $key => $node) {
            $value = $node->getValue();
            switch ($key) {
                case 0:
                    $this->assertSame(666, $value);
                    break;
                case 1:
                    $this->assertSame(11, $value);
                    break;
                case 2:
                    $this->assertSame(0, $value);
                    break;
                case 3:
                    $this->assertSame(-587, $value);
                    break;
            }
        }
    }

    public function testRemove(): void
    {
        $list = new IntegerSortedLinkedList();
        $list->add(11);
        $list->add(666);
        $list->add(-587);
        $list->add(0);

        $this->assertCount(4, $list);

        $list->remove(0);

        $this->assertCount(3, $list);

        $list->removeBulk([11, -587]);
        $this->assertCount(1, $list);
    }

    public function testRemoveAllNodesWithValue(): void
    {
        $setting = new Setting(removeAllNodesWithValue: true);
        $list = new IntegerSortedLinkedList($setting);
        $list->add(11);
        $list->add(66);
        $list->add(587);
        $list->add(66);

        $list->remove(66);

        $this->assertCount(2, $list);
    }

    public function testGetNodeByValue(): void
    {
        $list = new IntegerSortedLinkedList();
        $list->add(11);
        $list->add(666);
        $list->add(-587);
        $list->add(0);

        $bruno = $list->getNodeByValue(0);
        $this->assertInstanceOf(Node::class, $bruno);
        $this->assertSame(0, $bruno->getValue());

        $nonExistNode = $list->getNodeByValue('11');
        $this->assertNull($nonExistNode);
    }

    public function testUniqueValuesException(): void
    {
        $this->expectException(\LogicException::class);

        $setting = new Setting(uniqueValues: true);
        $list = new IntegerSortedLinkedList($setting);
        $list->add(10);
        $list->add(11);
        $list->add(11);
    }

}
