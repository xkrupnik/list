<?php

namespace SortedLinkedList;

use PHPUnit\Framework\TestCase;
use SortedLinkedList\Setting\Order;
use SortedLinkedList\Setting\Setting;
use SortedLinkedList\String\Node;

final class StringSortedLinkedListTest extends TestCase
{

    public function testCreatedWithValidValues(): void
    {
        $list = new StringSortedLinkedList();
        $list->add('rene');
        $list->add('adam');
        $list->add('žofie');
        $list->add('bruno');

        $this->assertSame(4, $list->count());

        foreach ($list as $node) {
            $this->assertInstanceOf(Node::class, $node);
            $this->assertIsString($node->getValue());
        }
    }

    public function testCreatedWithInvalidValues(): void
    {
        $this->expectException(\InvalidArgumentException::class);

        $list = new StringSortedLinkedList();
        $list->add('rene');
        $list->add('adam');
        $list->add(10);
        $list->add('bruno');
    }

    public function testOrderAsc(): void
    {
        $list = new StringSortedLinkedList();
        $list->add('rene');
        $list->add('adam');
        $list->add('žofie');
        $list->add('bruno');

        foreach ($list as $key => $node) {
            $value = $node->getValue();
            switch ($key) {
                case 0:
                    $this->assertSame('adam', $value);
                    break;
                case 1:
                    $this->assertSame('bruno', $value);
                    break;
                case 2:
                    $this->assertSame('rene', $value);
                    break;
                case 3:
                    $this->assertSame('žofie', $value);
                    break;
            }
        }
    }

    public function testOrderDesc(): void
    {
        $settings = new Setting(Order::descending);
        $list = new StringSortedLinkedList($settings);
        $list->add('rene');
        $list->add('adam');
        $list->add('žofie');
        $list->add('bruno');

        foreach ($list as $key => $node) {
            $value = $node->getValue();
            switch ($key) {
                case 0:
                    $this->assertSame('žofie', $value);
                    break;
                case 1:
                    $this->assertSame('rene', $value);
                    break;
                case 2:
                    $this->assertSame('bruno', $value);
                    break;
                case 3:
                    $this->assertSame('adam', $value);
                    break;
            }
        }
    }

    public function testRemove(): void
    {
        $list = new StringSortedLinkedList();
        $list->add('rene');
        $list->add('adam');
        $list->add('žofie');
        $list->add('bruno');

        $this->assertCount(4, $list);

        $list->remove('rene');

        $this->assertCount(3, $list);

        $list->removeBulk(['adam', 'bruno']);
        $this->assertCount(1, $list);
    }

    public function testRemoveAllNodesWithValue(): void
    {
        $setting = new Setting(removeAllNodesWithValue: true);
        $list = new StringSortedLinkedList($setting);
        $list->add('rene');
        $list->add('adam');
        $list->add('adam');
        $list->add('bruno');

        $list->remove('adam');

        $this->assertCount(2, $list);
    }

    public function testGetNodeByValue(): void
    {
        $list = new StringSortedLinkedList();
        $list->add('rene');
        $list->add('adam');
        $list->add('žofie');
        $list->add('bruno');

        $bruno = $list->getNodeByValue('bruno');
        $this->assertInstanceOf(Node::class, $bruno);
        $this->assertSame('bruno', $bruno->getValue());

        $nonExistNode = $list->getNodeByValue('sara');
        $this->assertNull($nonExistNode);
    }

    public function testUniqueValuesException(): void
    {
        $this->expectException(\LogicException::class);

        $setting = new Setting(uniqueValues: true);
        $list = new StringSortedLinkedList($setting);
        $list->add('rene');
        $list->add('adam');
        $list->add('adam');
    }

}
